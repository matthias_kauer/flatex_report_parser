# -*- coding: utf-8 -*-

# Copyright (C) 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
##
# @file flatex_parse.py
# @brief parses a directory of flatex pdf reports and collects all of them in an xls file
# @author Matthias Kauer
# @version 0.1
# @date 2013-07-01


import dateutil.parser as dparser
import re
import pandas as pd
import glob

from classify import classify_report
from translation import translation_dict
from pdfminer_front import *

BuySellList = ["Kurswert", "Einbeh. Steuer", "Endbetrag", "Provision", 
            "Eigene Spesen", "Fremde Spesen"]
DivList = ["pro Stück", 'Bruttodividende', 'Einbeh. Steuer', 'Endbetrag']
IntList = ["Ertrag", 'Zinsbetrag', 'Einbeh. Steuer', 'Endbetrag']

fp_num_re = r"-?\d+(?:.\d+)*(?:.\d+)?" #presumably fixed or floating point number regex (can't remember)
isin_wkn_regex = r'\([A-Z]{2}\w{10}/\w{6}\)'

def extract_EUR(target_pattern, instr):
    """ finds the EUR + float number fields in flatex reports and extracts the corresponding float

    @param target_pattern What word to look for, i.e. regex pattern: target_pattern EUR float
    @param instr the string to search for the pattern
    @return: the float that was found

    """
    #target pattern + space + EUR + space + anything + 2space or newline
    # regex = target_pattern + r'\s+EUR\s+.*(?=\n|\s{2})' 
    regex = target_pattern + r'\s+EUR\s+(' + fp_num_re + ').*(?=\n|\s{2})' 

    relist = re.findall(regex, instr)
    if relist:
        return GER_float_from_relist_to_float(relist)
    else:
        regex = target_pattern + r'\s+:\s+('+fp_num_re+').*(?=\n|\s{2})'
        relist = re.findall(regex, instr)
        return GER_float_from_relist_to_float(relist)

def GER_float_from_relist_to_float(relist):
    """ Converts a comma separated number (as found by regex) to a float, by transforming
    German numbers such that Python understands them, i.e. 1.234,00 becomes 1,234.00
    

    @param relist return from re.findall that contains a German float number
    @return: converted float number

    """
    if relist:
        val = relist[0].replace(',','xtemp').replace('.','').replace('xtemp','.')
    else:
        val = 0
    return float(val)
 
def extract_date(instr):
    """ Extracts the date from the Valuta field in the pdf parsed string
    

    @param instr the string that should be parsed
    @return: datetime object of the parsed date

    """
    regex = r"Valuta" + r'\s+\d{2}\.\d{2}\.\d{4}'
    relist = re.findall(regex, instr)
    date = dparser.parse(relist[0], fuzzy=True).date()
    return date

def extract_amount(instr):
    """ extracts the number of shares that the transaction concerns. sometimes after "ausgefuehrt", sometimes called "Nominale"

    @param instr the string that should be parsed
    @return: gross amount (as integer)

    """
    regex = r"Ausgeführt" + r'\s+(\d+)\s+St\.'
    relist = re.findall(regex, instr)
    if not relist:
        regex = r"St\.\s+:\s+(\d+)(?=\n|\s{2})"
        relist = re.findall(regex,instr)
    if not relist:
        # print instr
        regex = r"Nominale\s+:\s+("+fp_num_re+")\s+EUR"
        relist = re.findall(regex,instr)
    if not relist:
        regex = r"Ausgeführt\s+("+fp_num_re+")\s+EUR"
        relist = re.findall(regex,instr)

    amount = relist[0].replace('.','')
    amount = amount.replace(',', '.')
    return float(amount)

def extract_Price(instr):
    """ extracts the price of the parsed transaction
    

    @param instr the flatex pdf -> string that should be parsed
    @return: the price of the transaction

    """

    regex = r"Kurs" + r'\s+(\d+(?:.\d+)*(?:.\d+)?)\s+EUR'
    relist = re.findall(regex, instr)

    return GER_float_from_relist_to_float(relist)

def extract_isin_wkn(instr):
    """ extracs ISIN and WKN combined. Mainly a subroutine for extract_isin and extract_wkn
    

    @param instr string from flatex pdf -> string that is to be parsed
    @return: combined ISIN / WKN string

    """
    # relist = re.findall(r'\([A-Z]{2}\d{10}/\w{6}\)', instr) #this was not correct, some ISIN have letters in mid
    relist = re.findall(isin_wkn_regex, instr)

    return relist[0]

def extract_isin(instr):
    """ extracs ISIN
    

    @param instr string from flatex pdf -> string that is to be parsed
    @return: ISIN string
    """ 
    isin_wkn = extract_isin_wkn(instr)
    isin_regex = r'(?<=\()[A-Z]{2}\w{10}' #(?<=..) must be preceeded by ..
    relist = re.findall(isin_regex, isin_wkn)
    # relist = re.findall(r'(?<=\()[A-Z]{2}\d{10}', isin_wkn)
    return relist[0]

def extract_wkn(instr):
    """ extracs WKN
    

    @param instr string from flatex pdf -> string that is to be parsed
    @return: WKN string
    """ 
    isin_wkn = extract_isin_wkn(instr)
    relist = re.findall(r'(?<=/)\w{6}', isin_wkn)
    return relist[0]

def extract_name(instr):
    """ extracts the name of the security next to ISIN + WKN for more readability

    @param instr string from flatex pdf -> string that is to be parsed
    @return: name of the security in the transaction

    """
    # regex = r'\s{2}.+?\s+\([A-Z]{2}\d{10}/\w{6}\)'
    regex = r'\s{2}.+?\s+' + isin_wkn_regex
    relist1 = re.findall(regex, instr);
    
    relist = re.findall(r'\s{4}(.*)\s+', relist1[0])
    return relist[0].strip(' ')

def flatex_parse(statement_str, verbose=0):
    """ calls classifier for flatex statement string. then further parses this string based on its classification to
    construct a dictionary that contains the discovered data
    

    @param statement_str string from flatex pdf -> string converter (for entire pdf)
    @param verbose controls output print messages (verbose=0 is default and results in minimal output)
    @return: a dictionary listing the transactions one by one

    """
    statement_cls = classify_report(statement_str)
    if(verbose>0):
        print "statement_cls: " + statement_cls

    temp_dict={}
    out_dict = {}
    if statement_cls in ["SELL", "BUY"]:
        for key in BuySellList:
            temp_dict[key]= extract_EUR(key, statement_str)
        out_dict["Fees"] = temp_dict["Eigene Spesen"] \
                + temp_dict["Fremde Spesen"]\
                + temp_dict["Provision"]
    elif statement_cls in ["Dividend"]:
        for key in DivList:
            temp_dict[key] = extract_EUR(key, statement_str)
        out_dict["Fees"] = 0.0
    elif statement_cls in ["Interest"]:
        for key in IntList:
            temp_dict[key] = extract_EUR(key, statement_str)
    else:
        return None

    out_dict['Currency'] = 'EUR'
    out_dict['Type'] = statement_cls
    out_dict["Valuta"] = extract_date(statement_str)
    out_dict["Amount"] = extract_amount(statement_str)
    out_dict["Price"] = extract_Price(statement_str)
    out_dict["WKN"] = extract_wkn(statement_str)
    out_dict["ISIN"] =  extract_isin(statement_str)
    out_dict["Name"] = extract_name(statement_str)
    print temp_dict
    for key in translation_dict:
        if key in temp_dict:
            out_dict[translation_dict[key][0]] = translation_dict[key][1](temp_dict[key])

    print out_dict
    return out_dict

def createExcelFromPdfFiles(filepattern, out_xls_file, txt_folder='txt_temp'):
    """ Parses all the pdf files in the given filepattern and writes the classified transactions to out_xls_file.
    

    @param filepattern filepattern for glob, e.g. "reports/*.pdf"
    @param out_xls_file output xls file, will be created, probably overwritten
    @return: None

    """

    convert_pdf_folder_to_txt_folder(filepattern, txt_folder)
    # txt_pattern = re.sub('.pdf$', '.txt', filepattern)
    # txt_pattern = os.path.join(txt_folder, '*.pdf')
    createExcelFromTxtFiles(txt_folder, out_xls_file)

def createExcelFromTxtFiles(txt_folder, out_xls_file):
    txt_list = glob.glob(os.path.join(txt_folder, '*.txt'))
    # txt_list = glob.glob(txt_pattern)
    print txt_list

    total_list = []
    for txt_fpath in txt_list:
        with open(txt_fpath, 'r') as f:
            txt_content = f.read()

        # print txt_content
        div_out = flatex_parse(txt_content, verbose=1)
        # total_dict[div_out['Date']] = div_out
        if div_out:
            total_list.append(div_out)

    # print total_list
    df = pd.DataFrame(total_list)
    print(df)

    df.to_excel(out_xls_file);

if __name__ == '__main__':
    # pdfrep_list = createExcelFromPdfFiles('flatex_reports/*.pdf', "text.xls")
    # convert_pdf_folder_to_txt_folder('trouble_examples/*.pdf', 'txt_out')
    # createExcelFromPdfFiles('trouble_examples', "test.xls")
    createExcelFromPdfFiles('txt_examples', "test.xls")

