# Copyright (C) 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
##
# @file pdfminer_front.py
# @brief transforms a pdf to a string
# @author Matthias Kauer
# @version 0.1
# @date 2013-07-01


import glob
import re
import os
from pdfminer.pdfinterp import PDFResourceManager, process_pdf
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from cStringIO import StringIO

def convert_pdf(path):
    """ Convert a pdf file to a string that can be further processed in Python
    

    @param path A pdf file. Full path and local path should both work, but I only tested without path, i.e. local
    @return: string of text in the pdf file

    """
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)

    fp = file(path, 'rb')
    process_pdf(rsrcmgr, device, fp)
    fp.close()
    device.close()

    str = retstr.getvalue()
    retstr.close()
    return str


def convert_pdf_folder_to_txt_folder(pdf_folder, txt_folder):
    pdfrep_list = glob.glob(os.path.join(pdf_folder, '*.pdf'))

    if not os.path.exists(txt_folder):
        os.makedirs(txt_folder)

    for pdf_fpath in pdfrep_list:
        print pdf_fpath
        pdf_fname = os.path.basename(pdf_fpath)
        # txtrep = pdfrep.replace('.pdf', '.txt')
        txt_fpath = os.path.join(txt_folder,re.sub('.pdf$', '.txt', pdf_fname)) #should be safer when also checking for the end of string
        print txt_fpath
        txt_content = convert_pdf(pdf_fpath)

        with open(txt_fpath, 'w') as f:
            f.write(txt_content)

