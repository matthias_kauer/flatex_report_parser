# -*- coding: utf-8 -*-
import dateutil.parser as dparser

def convert_date(date_str):
    # i = iter(extract_block("Valuta", instr))
    date = dparser.parse(date_str, fuzzy=True).date()
    return date

id_fct = lambda x : x
to_percent = lambda x : float(x)/100.0

translation_dict = {'Bruttodividende': ["Gross",id_fct],
        'pro Stück': ['Price',id_fct],
        'Einbeh. Steuer': ["Tax", id_fct],
        'Valuta': ["Date", convert_date],
        'Endbetrag': ["Net", id_fct],
        'Zinsbetrag': ["Gross", id_fct],
        'Ertrag': ["per share", id_fct],
        'Ihre Depotnummer': ["Account#", id_fct],
        "Kurswert": ["Gross", id_fct],
        'Ertrag' : ['Price', to_percent]
        }

