# Copyright (C) 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
##
# @file classify.py
# @brief Classifies a flatex report that has been translated to a string via regular expression search.
# @author Matthias Kauer
# @version 0.1
# @date 2013-07-01



import re
class_dict = {'SELL': "^Wertpapierabrechnung Verkauf",
              'BUY': "^Wertpapierabrechnung Kauf",
              'Dividend': "^Dividendengutschrift",
              'Interest': "^Zinsgutschrift f.*r Wertpapiere",
              'AcctFee': "^Depotservicegeb.*hr",
              'AcctStatement': "^Kontoauszug Nr: ",
              'TaxRep': "^STEUERREPORT"}


def classify_report(txtstr):
    """ Classify a txtstr that comes from a converted flatex pdf into the various report types they offer
        according to class_dict. Relies on Python regex.
    

    @param txtstr A string as returned by pdfminer_front from a flatex report pdf file.
    @return: a class from class_dict or "undef"

    """
    for cls in class_dict:
        regex = class_dict[cls]
#        all_matches = re.findall(regex, txtstr)
        if re.search(regex, txtstr, flags=re.MULTILINE):
            return cls

    return "undef"


